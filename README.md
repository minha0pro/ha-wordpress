## Run WordPress:PHP-FPM + NGINX 

### Step 1: Setting server name

Change the server_name in the `config/nginx.conf` file

`server_name your_server_name www.your_server_name;` 

If you want to run on the local machine, then edit your /etc/hosts file by appending:

`127.0.0.1 your_server_name`

### Step 2: Local development. For production deployment, go to step 4

Build images

`docker-compose build`

Run containers

`docker-compose up -d`

### Step 3: Setup WordPress

Go to URL http://your_server_name/ and setup your WordPress

### Step 4: Building, publishing docker images and deploying containers 

```sh
cd nginx
docker build -t your_registry_host/your_nginx_image .
docker push your_registry_host/your_nginx_image:latest
```

```sh
cd wordpress
docker build -t your_registry_host/your_wordpress_image .
docker push your_registry_host/your_wordpress_image:latest
```

Create a file contains environment variables: `.env` file

```properties
WORDPRESS_DB_NAME=your_db_name
WORDPRESS_DB_HOST=your_db_host
WORDPRESS_DB_USER=your_db_user
WORDPRESS_DB_PASSWORD=your_db_password
REPLICA_DB_HOST=your_replica_host
REPLICA_DB_USER=your_replica_user
REPLICA_DB_PASSWORD=your_replica_password

REGISTRY_HOST=your_registry_host
APP_IMAGE=your_wordpress_image
WEB_IMAGE=your_nginx_image
```

Running by command

`docker-compose -f docker-compose-deployment.yml up -d`

Go to step 3