#!/bin/bash

for server in "${DEPLOY_SERVERS[@]}"; do
    ssh $DEPLOY_USER@$server "cd $APP_DIR && sudo docker-compose up -d"
done
